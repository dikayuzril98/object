import React, {Somponent} from 'react'
import {StyleSheet, View, Text} from 'react-native'

class ObjectPrototype extends Component {
    render(){

        function GameLife(name, energy){
            this.name = name
            this.energy = energy
        }

        GameLife.prototype.eat = function(Lot){
            this.energy += Lot
        }

        let gamer = new GameLife('Gamer 1', 10)
        gamer.eat (10)

        return(
            <View>
                <Text>Function In Object</Text>
                <Text>Gamer Name: {gamer.name}</Text>
                <Text>Gamer Energy: {gamer.energy}</Text>
            </View>
        )
    }
}
export default ObjectPrototype

const styles = StyleSheet.create({
    
})