import React, {Component} from 'react'
import {StyleSheet, View, Text} from 'react-native'

class FunctionInObject extends Component {
    render(){
        let gamerMethod = {
            eat: function(Lot){
                this.energy += Lot
            },
            play: function(hour){
                this.energy -= hour
            },
            sleep: function(hour){
                this.energy += hour * 2
            }
        }
        function GameLife(name, energy){
            let gamer = {}
            gamer.name = name
            gamer.energy = energy
            gamer.eat = gamerMethod.eat
            gamer.play = gamerMethod.play
            return gamer
        }
        let gamer = GameLife('Gamer 1', 10)
        gamer.eat(10)
        gamer.play(5)
        return(
            <View>
                <Text>Function In Object</Text>
                <Text>Gamer Name: {gamer.name}</Text>
                <Text>Gamer Energy {gamer.energy}</Text>
            </View>
        )
    }
}

export default FunctionInObject

const styles = StyleSheet.create({

})