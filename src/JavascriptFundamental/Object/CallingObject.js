import React, {Component} from 'react'
import {StyleSheet, View, Text} from 'react-native'

class CallingObject extends Component {
    render(){
        let student = {
            name: 'Student1',
            address: 'Address1',
            hobby: 'Codding',
            grade: {
                assignment1: 90,
                assignment2: 87,
                assignment3: 92
            },
            passing_grade: true
        }
        return(
            <View>
                <Text>Object Section</Text>
                <Text>Name : {student.name}</Text>
                <Text>Address : {student.address}</Text>
                <Text>Hobby : {student.hobby}</Text>
                <Text>assignment1 : {student.grade.assignment1}</Text>
                <Text>assignment2 : {student.grade.assignment2}</Text>
                <Text>assignment3 : {student.grade.assignment3}</Text>
                <Text>Passing Grade : {student.passing_grade?'Pass':'Not Pass'}</Text>
            </View>
        )
    }
}

export default CallingObject

const styles = StyleSheet.create({

})