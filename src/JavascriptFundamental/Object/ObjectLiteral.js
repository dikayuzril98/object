import React, {Component} from 'react'
import {StyleSheet, View, Text}from 'react-native'

class ObjectLiteral extends Component {
    render(){
        let student = {
            name: 'Student1',
            address: 'Address1',
            hobby: 'Codding',
            Grade: {
                assignment1: 90,
                assignment2: 87,
                assignment3: 92
            },
            passing_grade: true
        }
        return(
            <View>
                <Text>Object Section</Text>
                <Text>{JSON.stringify(student)}</Text>
            </View>
        )
    }
}

export default ObjectLiteral

const styles = StyleSheet.create({
    
})