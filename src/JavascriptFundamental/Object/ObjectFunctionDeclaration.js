import React, {Component} from 'react'
import {StyleSheet, View, Text} from 'react-native'

class ObjectFunctionDeclaration extends Component {
    render(){
       function studentObject(name, address, hobby, assignment1, assignment2, assignment3, passing_grade){
           let student = {}
           student.name = name
           student.address = address
           student.hobby = hobby
           student.grade = {
               assignment1: assignment1,
               assignment2: assignment2,
               assignment3: assignment3
           }
           student.passing_grade = passing_grade
           return student
       }
       let data = studentObject('Student1', 'Address1', 'Codding', 80, 90, 92, true)
       return(
           <View>
               <Text>Object Section</Text>
               <Text>Name : {data.name}</Text>
               <Text>Address : {data.address}</Text>
               <Text>Hobby : {data.hobby}</Text>
               <Text>Assignment1 : {data.grade.assignment1}</Text>
               <Text>Assignment2 : {data.grade.assignment2}</Text>
               <Text>Assignment3 : {data.grade.assignment3}</Text>
               <Text>Passing Grade : {data.passing_grade?'Pass':'Not Pass'}</Text>
           </View>
       )
    }
}

export default ObjectFunctionDeclaration

const styles = StyleSheet.create({

})
