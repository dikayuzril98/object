import React, {Component} from 'react'
import {StyleSheet, View, Text} from 'react-native'

class ObjectConstructor extends Component {
    render(){
        function studentObject(name, address, hobby, assignment1, assignment2, assignment3, passing_grade){
            this.name = name
            this.address = address
            this.hobby = hobby
            this.grade = {
                assignment1: assignment1,
                assignment2: assignment2,
                assignment3: assignment3
            }
            this.passing_grade = passing_grade

        }
        let data = new studentObject('Student1', 'Address1', 'Codding', 80, 90, 92, true)
        return(
            <View>
                <Text>Object Section</Text>
                <Text>Name : {data.name}</Text>
                <Text>Address : {data.address}</Text>
                <Text>Hobby : {data.hobby}</Text>
                <Text>Assignment1 : {data.grade.assignment1}</Text>
                <Text>Assignment2 : {data.grade.assignment2}</Text>
                <Text>Assignment3 : {data.grade.assignment3}</Text>
                <Text>Passing Grade : {data.passing_grade?'Pass':'Not Pass'}</Text>
            </View>
        )
    }
}

export default ObjectConstructor

const style = StyleSheet.create({

})