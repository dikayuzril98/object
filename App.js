import React, {Component} from 'react'
import {StyleSheet, View, Text} from 'react-native'
import CallingObject from './src/JavascriptFundamental/Object/CallingObject'
import FunctionInObject from './src/JavascriptFundamental/Object/FunctionInObject'
import ObjectConstructor from './src/JavascriptFundamental/Object/ObjectConstructor'
import ObjectCreate from './src/JavascriptFundamental/Object/ObjectCreate'
import ObjectFunctionDeclaration from './src/JavascriptFundamental/Object/ObjectFunctionDeclaration'
import ObjectLiteral from './src/JavascriptFundamental/Object/ObjectLiteral'
import ObjectPrototype from './src/JavascriptFundamental/Object/ObjectLiteral'

class App extends Component {
  render(){
    return(
      <View style={styles.container}>
        <Text >
          <FunctionInObject/>
        </Text>
      </View>
    )
  }
}
export default App

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  }
})